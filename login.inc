<div ng-app="myApp" ng-controller="myControlador">
	<input type="text" ng-model="iUsuario" placeholder="Usuario" />
	<input type="password" ng-model="iPass" placeholder="Password" />
	<button ng-click="logueo()">Aceptar</button>
	<br />
	{{respuestaLogin}}
</div>

<script>
	var app = angular.module("myApp", []);

	app.constant("credencialesAcceso",{
		"user": "Meny",
		"pass": "Entr4r."
	});

	app.factory("loginFactory", function(credencialesAcceso){
		var _loguear = function(user, pass){
				if(user==credencialesAcceso.user && pass==credencialesAcceso.pass)
					return true;
				else
					return false;
			}

		return { loguear: _loguear };
	});

	app.controller("myControlador", ["$scope", "loginFactory",
		function($scope, loginFactory){
			var elem = this;

			$scope.logueo = function(){
				if(elem.login = loginFactory.loguear($scope.iUsuario, $scope.iPass))
					$scope.respuestaLogin="Aacceso correcto!!!";
				else
					$scope.respuestaLogin="Error en usuario y/o contraseña";
			}
		}]
	);
</script>