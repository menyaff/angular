<?php
	if(isset($_REQUEST["accion"])){
		switch($_REQUEST["accion"]){
			case "create":
				if(isset($_POST["nombre"]))
					$resp = array("event"=>"SUCCESS","mensaje"=>"Registro ".$_POST["nombre"]." creado satisfactoriamente");
				else
					$resp = array("event"=>"FAIL","mensaje"=>"Error al crear registro");
				break;
			case "delete":
				$resp = array("event"=>"SUCCESS","mensaje"=>"Registro eliminado satisfactoriamente");
				break;
			case "select":
				$resp = array("event"=>"SUCCESS","mensaje"=>array(
							array("nombre"=>"Ejemplo 1","valor"=>"Dato 1"),
							array("nombre"=>"Ejemplo 2","valor"=>"Dato 2"),
							array("nombre"=>"Ejemplo 3","valor"=>"Dato 3"),
							array("nombre"=>"Ejemplo 4","valor"=>"Dato 4"),
							array("nombre"=>"Ejemplo final","valor"=>"Dato final")
						));
				break;
			default:
				$resp = array("event"=>"FAIL","mensaje"=>"Error al definir acción");
				break;
		}
	}else
		$resp = array("event"=>"FAIL","mensaje"=>"Falta definir acción");

	echo json_encode($resp);

	if(isset($_GET["front"])){
		echo "<br /><pre>";
		print_r($resp);
		echo "</pre>";
	}
?>