<?php include "firePHP/fb.php"; ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Angular</title>
		<script src="JS/angular.min.js"></script>		
	</head>
	<body>
		<?php
			$ejemplos = array(
							"Funcionalidad login (factory y controller)"=>"login",
							"Variables"=>"variables",
							"Data bind"=>"dataBind",
							"Directivas"=>"mensajeDirectivas",
							"Controlador y scope"=>"controller",
							"Filtros"=>"filtros",
							"HTTP Request"=>"http"
						);

			FB::info($ejemplos);

			echo "<ul>";
			foreach($ejemplos as $index=>$item)
				echo "<li><a href='".$_SERVER["PHP_SELF"]."?ejemplo=".$item."'>".$index."</a></li>";
			echo "</ul>";
			
			if(isset($_GET["ejemplo"]))
				include $_GET["ejemplo"].".inc";
		?>
	</body>
</html>