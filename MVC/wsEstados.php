<?php
	$datos = array(
			9=>array("nombre"=>"Durango","capital"=>"Durango","region"=>"Centro"),
			5=>array("nombre"=>"Chihuahua","capital"=>"Chihuahua","region"=>"Norte"),
			31=>array("nombre"=>"Zacatecas","capital"=>"Zacatecas","region"=>"Centro"),
			19=>array("nombre"=>"Oaxaca","capital"=>"Oaxaca de Juarez","region"=>"Sur"),
			23=>array("nombre"=>"San Luis Potosí","capital"=>"San Luis Potosí","region"=>"Centro"),
			7=>array("nombre"=>"Coahuila","capital"=>"Saltillo","region"=>"Norte"),
			25=>array("nombre"=>"Sonora","capital"=>"Hermosillo","region"=>"Norte"),
			26=>array("nombre"=>"Tabasco","capital"=>"Villa Hermosa","region"=>"Sur"),
			13=>array("nombre"=>"Jalisco","capital"=>"Guadalajara","region"=>"Centro")
		);

	if(isset($_REQUEST["id"]))
		echo json_encode($datos[$_REQUEST["id"]]);
	else
		echo json_encode($datos);
?>