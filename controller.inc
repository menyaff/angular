<div ng-app="myApp" ng-controller="myControl">
	Nombre: <input ng-model="iNombre" />
	Apellido: <input ng-model="iApellido" />
	<h1>{{iNombre}}</h1>
	<h2 ng-bind="iApellido"></h2>
</div>

<script>
	var app = angular.module('myApp', []);

	app.controller('myControl', function($scope) {
		$scope.iNombre = "Manuel";
		$scope.iApellido = "Flores";
	});
</script>
