<?php

namespace samplaravel\Http\Controllers;

use Illuminate\Http\Request;

class ComentariosController extends Controller
{
	public function __construct(){

	}

    public function index(){
    	$contenido = \samplaravel\Comentarios::All();

    	return view("comentarios.mensajes", ["titulo"=>"Mensajes"])->with("contenido",$contenido);
    }
    public function contacto(){
    	return view("comentarios.contacto", ["titulo"=>"Comentarios"]);
    }
    public function store(Request  $request){
    	\samplaravel\Comentarios::create([
    		"destinatario" => $request["iDestinatario"],
    		"remitente" => $request["iRemitente"],
    		"urgente" => isset($request["chUrgente"]) ? true : false,
    		"mensaje" => $request["taMensaje"]
    	]);

    	return redirect("/comentarios/mensajes")->with(["nuevo"=>true]);
    }
}
