<?php

namespace samplaravel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
	public function authenticate(){
    	if (Auth::attempt(['email' => $email, 'pass' => $password])) {

            return redirect("/comentarios/mensajes")->with(["titulo"=>"Mensajes"]);
        }else
        	return "NO";
    }
    public function logout(){
    	Auth::logout();

    	return view("login");
    }
}
