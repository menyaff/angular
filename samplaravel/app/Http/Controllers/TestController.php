<?php

namespace samplaravel\Http\Controllers;

class TestController extends Controller
{
    public function index()
    {
        return "Enviado desde TestController.php";
    }
    public function saludo($nombre)
    {
        return "Saludos ".$nombre." desde TestController.php";
    }
}
