<?php

namespace samplaravel;

use Illuminate\Database\Eloquent\Model;

class Comentarios extends Model
{
    protected $table = "comentarios";
    protected $fillable = ["remitente","destinatario","urgente","mensaje"];
}
