@extends("layouts.main")

@section("css")
	#formContacto>div{
		text-align: center;
		margin-bottom: 5px;
	}
	#formContacto{ margin: 30px auto; }
@stop

@section("contenido")
	{!! Form::open(["action"=>"ComentariosController@store","id"=>"formContacto","method"=>"post"]) !!}
	    <div class="form-group">
	    	{!! Form::text("iRemitente",null,["class"=>"form-control","placeholder"=>"Remitente"]) !!}
	    </div>
	    <div class="form-group">
	    	{!! Form::text("iDestinatario",null,["class"=>"form-control","placeholder"=>"Destinatario"]) !!}
	    </div>
	    <div class="form-group">	    	
	    	{!! Form::checkbox("chUrgente",null,false,["class"=>"form-control","id"=>"chUrgente"]) !!}
	    	{!! Form::label(null,"Urgente",["for"=>"chUrgente"]) !!}
	   </div>
	    <div class="form-group">
	    	{!! Form::textarea("taMensaje",null,["class"=>"form-control","placeholder"=>"Mensaje","cols"=>"30","rows"=>"10"]) !!}
	    </div>
	    <div class="form-group">
	    	{!! Form::submit("Aceptar",["class"=>"btn"]) !!}
	    </div>
	{!! Form::close() !!}
@stop