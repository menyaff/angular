@extends("layouts.main")

<?php $nuevo = Session::get("nuevo"); ?>

@section("css")
	#spnMsjNuevo{
		text-align: center;
		color: #00F;
	}
	.urgente{
		color: #F00;
		font-weight: bold;
	}
	.mensaje{
		background-color: #ccc;
		border: 1px solid #999;
	}
	#sectMensajes>article{ 
		padding: 10px 15px;
		border-bottom: 1px solid #999; 
	}
@stop

@section("contenido")
	@if($nuevo)
		<div class="alert alert-success alert-dismissable" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<b>Mensaje creado satisfactoriamente</b>
		</div>
	@endif

	<section id="sectMensajes">
		@foreach($contenido as $mensaje)
			<article>
				{{ $mensaje->momento }}<br />
				De: {{ $mensaje->remitente }} a: {{ $mensaje->destinatario }}
				<div class="mensaje{{ $mensaje->urgente ? ' urgente' : '' }}">{{ $mensaje->mensaje }}</div>
			</article>
		@endforeach
		<br />
		{{ link_to_action("ComentariosController@contacto", "Escribir mensaje") }}
	</section>
@stop