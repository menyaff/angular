<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Contacto</title>
	<script src="/js/jquery-1.12.1.min.js"></script>
	<style>
		header>h1{ text-align: center; }
		#formContacto>div{
			text-align: center;
			margin-bottom: 5px;
		}
		#formContacto{ margin: 30px auto; }
	</style>
</head>
<body>
	<header>
		<h1>LIBRO DE VISITAS</h1>
	</header>
	<section>
		<div>
			<form id="formContacto" action="" method="post">
				<div><input type="text" name="iRemitente" id="iRemitente" placeholder="Remitente"></div>
				<div><input type="text" name="iDestinatario" id="iDestinatario" placeholder="Destinatario"></div>
				<div>
					<input type="checkbox" name="chUrgente" id="chUrgente">
					<label for="chUrgente">Urgente</label>
				</div>
				<div><textarea name="taMensaje" id="taMensaje" cols="30" rows="10" placeholder="Mensaje"></textarea></div>
				<div><button id="btnAceptar" name="btnAceptar">Aceptar</button></div>
			</form>
		</div>
	</section>
</body>
</html>