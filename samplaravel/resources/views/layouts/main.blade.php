<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>{{ $titulo }}</title>
		{!! Html::style("css/bootstrap.min.css") !!}
		{!! Html::style("css/bootstrap-theme.min.css") !!}
		{!! Html::style("css/app.css") !!}
		<style>
			#divContenido{ 
				width: 50%; 
				margin: 0 auto;
			}
			@yield("css")
		</style>
	</head>
	<body>
		<header>
			<h1>LIBRO DE VISITAS</h1>
		</header>
		<div id="divContenido">	
			@yield("contenido")
		</div>

		{!! Html::script("js/jquery-1.12.1.min.js") !!}
		{!! Html::script("js/bootstrap.min.js") !!}
		@yield("js")
	</body>
</html>