<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::post("/login","LoginController@authenticate");
Route::get("/salir","LoginController@logout");
Route::get("saludo/{nombre?}",function($nombre="mundo"){
	return "Hola ".$nombre;
});
Route::get("controlador","TestController@index");
Route::get("controlador/{nombre}","TestController@saludo");
Route::resource("Atm","AtmController");
Route::get("comentarios",function(){
	return redirect("/comentarios/mensajes");
});
Route::get("comentarios/mensajes","ComentariosController@index");
Route::get("comentarios/contacto","ComentariosController@contacto");
Route::post("comentarios/crear","ComentariosController@store");
Auth::routes();

Route::get('/home', 'HomeController@index');
