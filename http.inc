<div ng-app="myApp" ng-controller="myControlador">
	<h2>EMPTY</h2>
	{{responseEmpty}}
	<hr />
	<h2>SELECT</h2>
	{{responseSelect}}
	<hr />
	<h2>CREATE 1</h2>
	{{responseCreate1}}
	<hr />
	<h2>CREATE 2</h2>
	{{responseCreate2}}
	<hr />
	<h2>DELETE</h2>
	{{responseDelete}}
</div>

<script src="JS/jquery-1.12.1.min.js"></script>
<script>
	var app = angular.module('myApp', []);

	app.controller("myControlador", function($scope, $http) {
		$http.get("webservice.php")
		.then(function(response) {
			$scope.responseEmpty = response.data;
		});

		$http.get("webservice.php", {params: {accion:"select"}}
		).then(function(response) {
			$scope.responseSelect = response.data;
		});

		$http({
			method: "GET",
			url: "webservice.php",
			params: {accion:"create"}
		}).then(function(response) {
			$scope.responseCreate1 = response.data;
		});

		$http({
			method: "POST",
			url: "webservice.php",
			data: $.param({accion:"create",nombre:"Meny"}),
    		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(response) {
			$scope.responseCreate2 = response.data;
		});

		$http({
			method: "GET",
			url: "webservice.php",
			params: {accion:"delete"}
		}).then(function(response) {
			$scope.responseDelete = response.data;
		});
	});
</script> 