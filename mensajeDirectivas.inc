<div ng-app="myApp" mensaje></div>
<script>
	var app = angular.module("myApp", []);

	app.directive("mensaje", function() {
	    return {
	        template : "Creado con la directiva de un constructor!"
	    };
	});
</script>