<div ng-app="myApp" ng-controller="myControlador">
	<h2>Lista completa</h2>
	<ul>
		<li ng-repeat="item in capitales">
			{{item.capital + ", " + item.estado}}
		</li>
	</ul>
	<hr />
	<h2>Lista ordenada</h2>
	<ul>
		<li ng-repeat="item in capitales | orderBy: 'estado'">
			{{item.capital + ", " + item.estado}}
		</li>
	</ul>
	<hr />
	<h2>Mayúsculas</h2>
	<ul>
		<li ng-repeat="item in capitales">
			{{item.capital | uppercase}}, {{item.estado | uppercase}}
		</li>
	</ul>
	<hr />
	<h2>Filtrar</h2>
	<input type="text" placeholder="Coincidencias" ng-model="iFiltro" />
	<ul>
		<li ng-repeat="item in capitales | filter: iFiltro">
			{{item.capital + ", " + item.estado}}
		</li>
	</ul>
</div>

<script>
	angular.module("myApp",[]).controller("myControlador", function($scope){
		$scope.capitales = [
					{estado:"Durango",capital:"Durango"},
					{estado:"Chihuahua",capital:"Chihuahua"},
					{estado:"Jalisco",capital:"Guadalajara"},
					{estado:"Nuevo León",capital:"Monterrey"},
					{estado:"Yucatán",capital:"Mérida"},
					{estado:"Coahuila",capital:"Torreón"},
					{estado:"Tabasco",capital:"Villahermosa"}
				];
	});
</script>